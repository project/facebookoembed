<?php

namespace Drupal\facebookoembed;
use Drupal\media\OEmbed;
use Drupal\Component\Utility\UrlHelper;



class FacebookoembedUrlResolver extends OEmbed\UrlResolver {


  /**
   * {@inheritdoc}
   */
  public function getResourceUrl($url, $max_width = NULL, $max_height = NULL) {
    // Try to get the resource URL from the static cache.
    if (isset($this->urlCache[$url])) {
      return $this->urlCache[$url];
    }

    // Try to get the resource URL from the persistent cache.
    $cache_id = "media:oembed_resource_url:$url:$max_width:$max_height";

    $cached = $this->cacheGet($cache_id);
    if ($cached) {
      $this->urlCache[$url] = $cached->data;
      return $this->urlCache[$url];
    }

    $provider = $this->getProviderByUrl($url);
    $endpoints = $provider->getEndpoints();

    if ($provider->getName()=="Facebook") {
        foreach($endpoints as $key=>$endpoint) {
            $find=strpos($endpoint->getUrl(), "video");
            if (gettype($find)!=="boolean" && $find>=0) {
               $endpoints=[$endpoint];
               break;
            }
        }
      }

    $endpoint = reset($endpoints);


    $resource_url = $endpoint->buildResourceUrl($url);

    $parsed_url = UrlHelper::parse($resource_url);
    if ($max_width) {
      $parsed_url['query']['maxwidth'] = $max_width;
    }
    if ($max_height) {
      $parsed_url['query']['maxheight'] = $max_height;
    }
    // Let other modules alter the resource URL, because some oEmbed providers
    // provide extra parameters in the query string. For example, Instagram also
    // supports the 'omitscript' parameter.
    $this->moduleHandler->alter('oembed_resource_url', $parsed_url, $provider);
    $resource_url = $parsed_url['path'] . '?' . rawurldecode(UrlHelper::buildQuery($parsed_url['query']));

    $this->urlCache[$url] = $resource_url;
    $this->cacheSet($cache_id, $resource_url);

    return $resource_url;
  }


}

