<?php
namespace Drupal\facebookoembed;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;

class FacebookoembedServiceProvider extends ServiceProviderBase Implements ServiceProviderInterface {
    public function alter(ContainerBuilder $container) {
        $definition = $container->getDefinition('media.oembed.url_resolver');
        $definition->setClass('Drupal\facebookoembed\FacebookoembedUrlResolver');
        $definition = $container->getDefinition('media.oembed.resource_fetcher');
        $definition->setClass('Drupal\facebookoembed\FacebookoembedResourceFetcher');
    }
}

