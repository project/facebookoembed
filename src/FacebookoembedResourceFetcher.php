<?php

namespace Drupal\facebookoembed;

use Drupal\media\OEmbed;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\RequestException;
use Drupal\media\OEmbed\ResourceException;

class FacebookoembedResourceFetcher extends OEmbed\ResourceFetcher {
  private function getIdFromInput($url) {
    preg_match('/^https?:\/\/(www\.)?facebook.com\/([\w-\.]*\/videos\/|video\.php\?v\=)(?<id>[0-9]*)\/?$/', $url, $matches);
    if (!isset($matches['id'])) {
        preg_match('/^(https?:\/\/(www\.)?facebook.com\/([\w-\.]*\/videos\/|video\.php\?v\=)vb.[0-9]*\/)(?<id>[0-9]*)(.*)$/', $url, $matches);
        if (!isset($matches['id'])) {
            $lastslash=strrpos($url,'/');
            if ($lastslash!==false && $lastslash>=0) {
                if ($lastslash==strlen($url)-1) {
                    $url=substr($url,0,$lastslash);
                    $lastslash=strrpos($url,'/');
                }
                $intmark=strrpos($url,'?');
                if ($intmark!==false && $intmark>=0) {
                    return substr($url,$lastslash+1,$intmark-$lastslash-1);
                } else {
                    return substr($url,$lastslash+1);
                }
            }
        }
    }
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }
  private function getRemoteThumbnailUrl($id) {
    return sprintf('https://graph.facebook.com/%d/picture', $id);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchResource($url) {
    global $base_url;
    $cache_id = "media:oembed_resource:$url";

    $cached = $this->cacheGet($cache_id);

    if ($cached) {
      return $this->createResource($cached->data, $url);
    }

    try {
      $response = $this->httpClient->get($url);
    }
    catch (RequestException $e) {
      throw new ResourceException('Could not retrieve the oEmbed resource.', $url, [], $e);
    }

    list($format) = $response->getHeader('Content-Type');
    $content = (string) $response->getBody();
    if (strstr($format, 'text/xml') || strstr($format, 'application/xml')) {
      $data = $this->parseResourceXml($content, $url);
    }
    elseif (strstr($format, 'text/javascript') || strstr($format, 'application/json')) {
      $data = Json::decode($content);
    }
    // If the response is neither XML nor JSON, we are in bat country.
    else {
      throw new ResourceException('The fetched resource did not have a valid Content-Type header.', $url);
    }
    if ($data["provider_name"]=="Facebook") {
        $data["title"]=$this->getIdFromInput($data["url"]);
        $html=$data["html"];
        $atag=strpos($html,"<a");
        if ($atag!==false && $atag>=0) {
            $closea=strpos(substr($html,$atag+1),">");
            if ($closea!==false && $closea>=0) {
                $endtag=strpos(substr(substr($html,$atag+1),$closea+1),"<");
                if ($endtag!==false && $endtag>=0) {
                    $data["title"]=str_replace('>','&gt;',str_replace('<','&lt;',substr(substr(substr($html,$atag+1),$closea+1),0,$endtag)));
                }
            }
        }



        $data["thumbnail_url"]=$base_url.'/'.drupal_get_path('module', 'facebookoembed')."/img/videothumb.png";
        $imgdims=getimagesize($data["thumbnail_url"]);
        $data["thumbnail_width"]=$imgdims[0];
        $data["thumbnail_height"]=$imgdims[1];
        $data["description"]=$data["html"];
        $tmburl="https://www.facebook.com/video/embed?video_id=".$this->getIdFromInput($data["url"]);
        try {
            $thmbresponse = $this->httpClient->get($tmburl);
            $content = (string) $thmbresponse->getBody();
            $content=substr($content,strpos($content,"<img"));
            $content=substr($content,strpos($content,'style="')+7);
            $content=substr($content,strpos($content,'url(&#039;')+10);
            $content=substr($content,0,strpos($content,'&#039;'));

            while (strpos($content,'\\')) {
                $code=substr($content,strpos($content,'\\')+1,2);
                $content=substr($content,0,strpos($content,'\\'))."%$code".substr($content,strpos($content,'\\')+4);
            }
            $thumburl= urldecode($content)."&imagetype=image.jpg";
            $imgdims=getimagesize($thumburl);
            $data["thumbnail_url"]=$thumburl;
            $data["thumbnail_width"]=$imgdims[0];
            $data["thumbnail_height"]=$imgdims[1];
        }
        catch (Exception $e) {
        }

    }

    $this->cacheSet($cache_id, $data);

    return $this->createResource($data, $url);
  }


}

